#include <stdio.h>
#include <stdlib.h>
#include <start.h>
#include <game.h>
#include <global_def.h>
#include <unistd.h>


int main()
{
    /* initialisation */
    if (debug_start || debug_all)
    	printf("main v1.1\n");

    if (init())
    	play();
    printf ("\n\nBye !!!");
}

int init(){

	if (debug_start || debug_all)
		printf("Main Start\n");
    return LoadGame(IsGameSaved());


}


void play(){

	if (IsGameIsLoaded()){
		//printf ("Play ....  \n");
		char key;
		int quit=0;

        //display();

		do {
			//mycls();
			//

			key = getch();
			//printf("key %c \n", key);
			switch (key) {
			case MOVE_UP : game_move_up();break;
			case MOVE_DOWN : game_move_down();break;
			case MOVE_RIGHT : game_move_right();break;
			case MOVE_LEFT :  game_move_left();break;
			case KEY_MENU_QUIT : quit=TRUE;break;	/* Flag to exit Game */
			case KEY_MENU_RELOAD : if (!init()) quit=TRUE;break; /* If init() failed then quit */
			}
			if(gameIsFinish()){

				/* Save current Level */
				SaveGame();
				/* Load new Level */
				if (!init())
					quit=TRUE;
				/* Load a new Level */
			}
		}while(!quit);
	}
	else
		printf ("game not loaded\n");
}





