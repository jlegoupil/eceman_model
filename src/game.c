#include <stdio.h>
#include <stdlib.h>
#include <game.h>
#include <global_def.h>
#include <string.h>

/*
*   IN :
*   OUT : The game level
*/

/* game is ploaded in RAM, only visible for all this module */
static char *loadedGame = NULL;
static int bufferGameSize = 0, finish;
static Position currentPos;



/*
 * Check if a level is loaded in ram
 */
int IsGameIsLoaded(){
	if (loadedGame != NULL)
		return TRUE;
	return FALSE;
}

/* Save the current level to restart later */
void SaveGame(){

    FILE * saveFile = NULL;

	/*Get the current Level */
	char level = IsGameSaved();
	/* Next Level */
	level++;

	/*
	 *  Open de save file  then increase the current Level, then save it
	 */
	saveFile = fopen(SAVE_FILE,"w");
	/* write 1character to the save file */
	fwrite((const void *)&level,sizeof(char),1,saveFile);
	if (fclose(saveFile))
		printf("Game Saved Successfully\n");
}
/*
 * Check if a previous game is saved
 */
char IsGameSaved(){

    char* ligne=(char*)malloc(LINE_SIZE*sizeof(char)),*ptr_line;
    char level='0';
    int resRead;
    FILE * savefile = NULL;

    /* Ouverture u fichier de sauvegarde en lecture seule
    *  On recup�re le numero de tableau
    */
    savefile = fopen(SAVE_FILE,"r");
    /* Test si un fichier de sauvegarde existe */
    if (savefile){
        /* OUI : Lecture du numero de tableau */
        if ((resRead = fread(ligne,sizeof(char),1,savefile))  == 1){

        /* test the level is a number code sur 1 caracter*/
        ptr_line = ligne;
		if(*ptr_line <= '9' && *ptr_line >= '0'){
            level = (int)(*ptr_line - '0');
		}
        }
        /* close the file */
        fclose(savefile);
    }
    if (debug_game || debug_all)
    	printf("IsGameSaved Return level : %d \n",level-'0');
    return (level);
}

/*
*   IN :  Level
*   OUT : True is succeed, FALSE otherway
*/
int LoadGame(char level){
    /* Open the file matching with the level */
    char filename[FILENAME_SIZE], *message;
    FILE * file_id = NULL;
    int file_size;

    /* Initialisation */
    filename[0]='\0';
    finish = 0;


    if (debug_game)
       	printf("Game IN\n");

    /* Initialisation du pointeur sur le ebut du nom zone de stockage */

    /* construction du nom de fichier de reference */
    strcat (filename, GAME_FILE );
    /* construction du nom de fichier du niveau avec le niveau */
    strncat (filename, (char *)&level,1 );

    /* Add ext to the filename */
    strcat (filename , GAME_FILE_EXT);

    if (debug_game)
            printf("Game filename  : %s\n",filename);

    file_id = fopen(filename,"r");

    if (debug_game)
               printf("Game filename file_id  : %p\n",file_id);
    if (file_id != NULL){
        //seek the last byte of the file
       fseek(file_id,0,SEEK_END);
       //offset from the first to the last byte, or in other words, filesize
       file_size = ftell (file_id);
       if (debug_game)
               printf("Game filename filesize : %d\n",file_size);
       //go back to the start of the file
       rewind(file_id);

        //allocate a buffer that can hold it all
       loadedGame = (char*) malloc (sizeof(char) * (file_size + 1) );
       //read  all in one operation
       bufferGameSize = fread(loadedGame,sizeof(char),file_size,file_id);

       // Affichage
    	display();

       //and buffer is now officialy a string
       loadedGame[file_size] = '\0';
       fclose(file_id);
       if (! bufferGameSize) {
           //something went wrong, throw away the memory and set
           //the buffer to NULL
           free(loadedGame);
           /* Error message : Level doesn't exist */
            sscanf((char *)message,"Program level %c Error reading file\n",(char *)&level);
            ErrorMessage(message);
            return (FALSE);
        }
       else{
    	   if (debug_game)
    	           printf("Game file %s load SUCCESSFULLY  !!!!!\n",filename);
    	   /* Detect line, and pos */
    	   initPosition();
       }
    }
    else {
    	if (debug_game)
    	            printf("Game :  file %s not exist\n",filename);
        /* Error message : Level doesn't exist */
        ErrorMessage("test");
        return (FALSE);
    }
    return TRUE;
}



/*
 * Set the current position and width of the current game
 */
static void initPosition(){
	char *ptr = loadedGame;
	int pos_x=0, pos_y=0;

	if (debug_game)
		printf(">>>initPosition IN\n");
	/* Check all the game */
	while (*ptr != '\0'){
		if (debug_game)
            printf(" -> %c ",*ptr);
		switch  (*ptr++) {

		case '\n' :
			/* New Line detect the width's game */
			if (debug_game)
				printf("initPosition : New line detected pos (%d-%d)\n",pos_x,pos_y);
			if (!pos_y)
				currentPos.width = pos_x;
			pos_x = 0;
			pos_y++;

			break;

		case 'O' :
			if (debug_game)
				printf("initPosition : Start position detected\n");
			/* Start position detected : Set static currentPos*/
			currentPos.pos_x = pos_x;
			currentPos.pos_y = pos_y;
			if (debug_game){
				printf("Game:position X (%d) Y (%d)\n",currentPos.pos_x, currentPos.pos_y);
			}
			break;
		default: pos_x++;
		}
	}
	if (debug_game)
			printf("<<<initPosition OUT\n");
	// Set the cursor to ,the correct position
    gotoligcol(currentPos.pos_y,currentPos.pos_x);
}



/*
 * Display the current playing game
 */
 void display(){
	 char *ptr;
	 ptr = loadedGame;
	 while (*ptr != '\0')
		 printf("%c",*ptr++);
	 if (debug_game)
        printf("\n'u'->Up 'd'->Down 'l'->Left 'r'->Right ::: %s-> quit %s->Reload\n",STRING_MENU_QUIT,STRING_MENU_RELOAD);
}

 /* current position is returned */
#if 0
    Position getCurrentPosition(){
    if (debug_game)
		 printf("getCurrentPosition: X (%d) Y (%d)\n",currentPos.pos_x, currentPos.pos_y);
	return currentPos;
}
#endif

/* Check if position is available if true, return target position*/
static int checkPosIsFree(Position target){
	char *ptr = loadedGame, *ptr_c;
	ptr_c = ptr;
	int savex, savey;

#if 0
	//Position current = getCurrentPosition();
#endif
	/* current position */
	ptr_c += currentPos.pos_x + currentPos.pos_y*currentPos.width;
	if (debug_special){
        savex= currentPos.pos_x;
        savey= currentPos.pos_y;
        gotoligcol(28,1);
		printf("checkPosIsFree:c posx %d - c posy %d lar:%d (somme %d)\n",currentPos.pos_x,currentPos.pos_y,currentPos.width,currentPos.pos_x + currentPos.pos_y*currentPos.width);
		gotoligcol(savey,savex);
		}
	/* goto the target position */
	ptr += target.pos_x + target.pos_y*(1+target.width);
	if (debug_special){
            savex= currentPos.pos_x;
        savey= currentPos.pos_y;
        gotoligcol(29,1);
			printf("checkPosIsFree:t posx %d - t posy %d \n",target.pos_x,target.pos_y);
			printf("*ptr target %c\n",*ptr);
			gotoligcol(savey,savex);
	}
	switch  (*ptr) {
		case '*' :
		case 'o' :
		case '-' :
        	return FALSE;
		case 'X' :
			finish = TRUE;
			return TRUE;
		default :
		    /* Update the Memory map with 'occuped' */
            *ptr='o';
		    return TRUE;
	}
}
/*
  * Move the player up
  */
void game_move_up(){
	//getCurrentPosition();
	/* change the current position */
	setPos(UP);
}

/*
  * Move the player down
  */
void game_move_down(){
	//getCurrentPosition();
	/* change the current position */
	setPos(DOWN);
}

/*
  * Move the player left
  */
void game_move_left(){
	//getCurrentPosition();
	/* change the current position */
	setPos(LEFT);
}

/*
  * Move the player up
  */
void game_move_right(){
	//getCurrentPosition();
	/* change the current position */
	setPos(RIGHT);
}

int gameIsFinish(){
	return finish;
}



/* Check if position up to the current one is free */
static void setPos(Move direction){
	Position target;

	/* Duplicate current position */
	memcpy((void *)&target,(void *)&currentPos,sizeof(Position));
	if (debug_game){
            gotoligcol(25,1);
				printf("checkPosIsFree:c posx %d - c posy %d \n",target.pos_x,target.pos_y);
	}
	/* set target line up */
	switch (direction){
	case UP: target.pos_y--;break;//X--
	case DOWN: target.pos_y++;break;//X++
	case RIGHT: target.pos_x++;break;//Y++
	case LEFT: target.pos_x--;break;//Y--
	}
	if (debug_game){
            gotoligcol(26,1);
		printf("checkPosIsFree:t posx %d - t posy %d \n",target.pos_x,target.pos_y);
	}
	if (checkPosIsFree(target)){
		/* Update the current position */
        printf("o");
		memcpy((void *)&currentPos,(void *)&target,sizeof(Position));
		gotoligcol(currentPos.pos_y,currentPos.pos_x);
		printf("O");
	}
}

void ErrorMessage(char * mess){
    printf("\n\n\t\t ************** Critical Error  \n" );
    printf("%s",mess);
}

