#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED



/* CONSTANTES */
#define SAVE_FILE "save/save.txt"   	/* Nom du fichier de sauvegarde */
#define GAME_FILE "f:/level/game"   		/* Nom du fichier de sauvegarde */
#define GAME_FILE_EXT ".level"   		/* ext du fichier de sauvegarde */
#define LINE_SIZE 80 					/* Longueur d'une ligne texte 	*/
#define FILENAME_SIZE 50
#define MESSAGE_LENGTH 30

typedef struct Position_ {
	int pos_x;
	int pos_y;
	int width;
} Position;

typedef enum Move {
	UP, DOWN, RIGHT, LEFT} Move;

	/* Define keys for navigation */
#define MOVE_UP 	72
#define MOVE_DOWN	80
#define MOVE_RIGHT		77
#define MOVE_LEFT	75
	/* Define keys managing the game */
#define KEY_MENU_QUIT	'q'
#define KEY_MENU_RELOAD	'r'
#define STRING_MENU_QUIT	"'q'"
#define STRING_MENU_RELOAD	"'r'"

#define debug_game 0
#define debug_special 1



	char IsGameSaved(void);
	int IsGameIsLoaded(void);
	static void setPos(Move);
	//static int getPosFinish(Position);
	static void initPosition(void);
	//Position getCurrentPosition(void);

	int gameIsFinish(void);
	void game_move_up(void);
	void game_move_down(void);
	void game_move_right(void);
	void game_move_left(void);

	int LoadGame(char);
	void ErrorMessage(char * );
	void display(void);
	int gameIsFinish(void);
	void SaveGame();

#endif // GAME_H_INCLUDED
